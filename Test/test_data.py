import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

executable_path = "Test/driver/chromedriver.exe"
url = "https://sso.eservices.jud.ct.gov/foreclosures/Public/PendPostbyTownList.aspx"
element1 = "New Milford"
element2 = "Norwalk"
element3 = "Stamford"
element4 = "Shelton"
element5 = "Fairfield"
viewnotice = "//a[text()='View Full Notice']"


class TestSample():
    def test_task1(self):
        self.driver = webdriver.Chrome(executable_path)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()
        self.driver.get(url)
        self.driver.find_element_by_link_text(element1).click()
        print("Click on New Milford City")
        link1 = self.driver.find_element_by_xpath(viewnotice)
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL).click(link1).perform()
        print("View Full Notice In Diffrent TaB")
        self.driver.switch_to.window(self.driver.window_handles[-1])
        print("Switch to Next Tab")
        time.sleep(5)

    def test_task2(self):
        self.driver = webdriver.Chrome(executable_path)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()
        self.driver.get(url)
        self.driver.find_element_by_link_text(element2).click()
        print("Click on Norwalk")
        link1 = self.driver.find_element_by_xpath(viewnotice)
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL).click(link1).perform()
        print("View Full Notice In Diffrent TaB")
        self.driver.switch_to.window(self.driver.window_handles[-1])
        print("Switch to Next Tab")
        time.sleep(5)

    def test_task3(self):
        self.driver = webdriver.Chrome(executable_path)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()
        self.driver.get(url)
        self.driver.find_element_by_link_text(element3).click()
        print("Click on Stamford City")
        link1 = self.driver.find_element_by_xpath(viewnotice)
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL).click(link1).perform()
        print("View Full Notice In Diffrent TaB")
        self.driver.switch_to.window(self.driver.window_handles[-1])
        print("Switch to Next Tab")
        time.sleep(5)

    def test_task4(self):
        self.driver = webdriver.Chrome(executable_path)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()
        self.driver.get(url)
        self.driver.find_element_by_link_text(element4).click()
        print("Click on Shelton City")
        link1 = self.driver.find_element_by_xpath(viewnotice)
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL).click(link1).perform()
        print("View Full Notice In Diffrent TaB")
        self.driver.switch_to.window(self.driver.window_handles[-1])
        print("Switch to Next Tab")
        time.sleep(5)

    def test_task5(self):
        self.driver = webdriver.Chrome(executable_path)
        self.driver.implicitly_wait(50)
        self.driver.maximize_window()
        self.driver.get(url)
        self.driver.find_element_by_link_text(element5).click()
        print("Click on Fairfield City")
        link1 = self.driver.find_element_by_xpath(viewnotice)
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL).click(link1).perform()
        print("View Full Notice In Diffrent TaB")
        self.driver.switch_to.window(self.driver.window_handles[-1])
        print("Switch to Next Tab")
        time.sleep(5)
